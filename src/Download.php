<?php

declare(strict_types=1);

namespace Paneric\Download;

class Download
{
    private const CONTENT_TYPES = [
        'exe'  => 'application/octet-stream',
        'apk'  => 'application/octet-stream',
        'zip'  => 'application/zip',
        'mp3'  => 'audio/mpeg',
        'mpg'  => 'video/mpeg',
        'avi'  => 'video/x-msvideo',
        'pdf'  => 'application/pdf',
        'txt'  => 'text/plain',
        'html' => 'text/html',
        'htm'  => 'text/html',
        'doc'  => 'application/msword',
        'xls'  => 'application/vnd.ms-excel',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'gif'  => 'image/gif',
        'png'  => 'image/png',
        'jpeg' => 'image/jpg',
        'jpg'  => 'image/jpg',
        'php'  => 'text/plain'
    ];

    private const CONTENT_TYPE_DEFAULT = 'application/octet-stream';
    private const STATUS_CODE_200 = 200;

    private $headers = [];
    private $statusCode = 200;
    private $filePath;
    private $seek;

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function getSeek(): array
    {
        return $this->seek;
    }

    public function setHeaders(string $filePath, bool $isStream, string $httpRange): void
    {
        if (headers_sent()) {
            throw new \RuntimeException('Headers were already sent. Sending file not possible.');
        }

        if (!$filePath) {// invalid file path
            $this->statusCode = 400;
            return;
        }

        if (!is_file($filePath)) {// file does not exist
            $this->statusCode = 404;
            return;
        }

        $this->prepareApacheServer();

        $fileInfo = $this->prepareFileInfo($filePath);

        $headers = $this->initHeaders($fileInfo['file_name']);

        $headers = $this->setContentDisposition($headers, $fileInfo['file_name'], $isStream);
        $headers = $this->setContentType($headers, $fileInfo['file_ext']);

        $range = $this->prepareRange($httpRange);

        if ($this->statusCode === self::STATUS_CODE_200) {
            $seek = $this->prepareSeek($range, $fileInfo['file_size']);

            $headers = $this->setContentRange($headers, $fileInfo['file_size'], $seek);

            $this->filePath = $filePath;
            $this->seek = $seek;
        }

        $this->headers = $headers;
    }

    private function prepareApacheServer(): void
    {
        @ini_set('error_reporting', (string) (E_ALL & ~ E_NOTICE));
//        @apache_setenv('no-gzip', 1);//- turn off compression on the server
        @ini_set('zlib.output_compression', 'Off');
    }

    private function prepareFileInfo(string $filePath): array
    {
        $pathParts = pathinfo($filePath);

        return [
            'file_name' => $pathParts['basename'],
            'file_ext'  => $pathParts['extension'],
            'file_size' => filesize($filePath),
        ];
    }

    private function initHeaders(string $fileName): array
    {
        $headers['Pragma'] = 'public';
        $headers['Expires'] = '-1';
        $headers['Cache-Control'] = 'public, must-revalidate, post-check=0, pre-check=0';
        $headers['Content-Disposition'] = 'attachment; filename="'.$fileName.'"';

        return $headers;
    }

    private function setContentDisposition(array $headers, string $fileName, bool $isStream): array
    {
        if ($isStream) {
            $headers['Content-Disposition'] = 'inline;';
            $headers['Content-Transfer-Encoding'] = 'binary';

            return $headers;
        }

        $headers['Content-Disposition'] = 'attachment; filename="'.$fileName.'"';

        return $headers;
    }

    private function setContentType(array $headers, string $fileExt): array
    {
        $headers['Content-Type'] = self::CONTENT_TYPES[$fileExt] ?? self::CONTENT_TYPE_DEFAULT;

        return $headers;
    }

    private function prepareRange(string $httpRange): string
    {
        $range = '';

        if ($httpRange) {
            [$sizeUnit, $rangeOrig] = explode('=', $httpRange, 2);

            if ($sizeUnit === 'bytes') {
                [$range, $extraRanges] = explode(',', $rangeOrig, 2);
            } else {
                $this->statusCode = 416;
            }
        }

        return $range;
    }

    private function prepareSeek(string $range, int $fileSize): array
    {
        [$seekStart, $seekEnd] = explode('-', $range, 2);

        $seek['end']   = empty($seekEnd) ? ($fileSize - 1) : min(abs((int) $seekEnd), $fileSize - 1);
        $seek['start'] = (empty($seekStart) || $seekEnd < abs((int) $seekStart)) ? 0 : max(abs((int) $seekStart), 0);

        return $seek;
    }

    private function setContentRange(array $headers, int $fileSize, array $seek): array
    {
        if ($seek['start'] > 0 || $seek['end'] < ($fileSize - 1)) {
            $this->statusCode = 206;
            $headers['Content-Range'] = $seek['start'].'-'.$seek['end'].'/'.$fileSize;
            $headers['Content-Length'] = (string) ($seek['end'] - $seek['start'] + 1);
        } else {
            $headers['Content-Length'] = $fileSize;
        }

        $headers['Accept-Ranges'] = 'bytes';

        return $headers;
    }
}
